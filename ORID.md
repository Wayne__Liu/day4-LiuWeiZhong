## **O**：

- In today's Code Review, I found some flaws in my code, such as some test cases not passing due to my unclear requirements for tasks. Later, I also found that my code does not exhibit as much polymorphism as some other classmates. I also have a certain understanding and learning of the Observer pattern.

- I also learned some content about TDD. Testing is indeed an important step in the development process, and how to use TDD well during work is also a challenge. Unit testing is often the lowest cost way to discover and solve problems. The small step submission in the practice also provides us with a deeper understanding of the impact of TDD on development.

  

## **R：**

- I have a tight learning schedule, and each day is filled with different knowledge points. I hope to connect the knowledge I have learned in the past few days and truly use it in my work and study.


## **I：**

- Nice


## **D：**

- On weekends, we can give some exercises that summarize the knowledge points of the week, or give some time for ourselves to review.

