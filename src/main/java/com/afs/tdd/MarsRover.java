package com.afs.tdd;

import java.util.List;

public class MarsRover {

    private Location location;
    public MarsRover(Location location) {
        this.location = location;
    }

    public Location executeCommand(Command commandMove) {
        if (commandMove.equals(Command.Move)) {
            if (location.getDirection().equals(Direction.North)) {
                location.setCoordinateY(location.getCoordinateY() + 1);
            }else if(location.getDirection().equals(Direction.South)){
                location.setCoordinateY(location.getCoordinateY()-1);
            }else if(location.getDirection().equals(Direction.East)){
                location.setCoordinateX(location.getCoordinateX()+1);
            }else if(location.getDirection().equals(Direction.West)){
                location.setCoordinateX(location.getCoordinateX()-1);
            }
            return location;
        }else if(commandMove.equals(Command.Left)) {
            if (location.getDirection().equals(Direction.North)) {
                location.setDirection(Direction.West);
            }else if(location.getDirection().equals(Direction.East)) {
                location.setDirection(Direction.North);
            }else if(location.getDirection().equals(Direction.South)){
                location.setDirection(Direction.East);
            }else if(location.getDirection().equals(Direction.West)){
                location.setDirection(Direction.South);
            }
            return location;
        }else {
            if (location.getDirection().equals(Direction.North)) {
                location.setDirection(Direction.East);
            }else if(location.getDirection().equals(Direction.East)){
                location.setDirection(Direction.South);
            }else if(location.getDirection().equals(Direction.South)){
                location.setDirection(Direction.West);
            }else if(location.getDirection().equals(Direction.West)){
                location.setDirection(Direction.North);
            }
            return location;
        }
    }
    public Location executeBatchCommand(List<Command> commands){
        for (Command command : commands){
            executeCommand(command);
        }
        return location;
    }
}
