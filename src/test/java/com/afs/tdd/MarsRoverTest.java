package com.afs.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class MarsRoverTest {

    @Test
    void should_change_to_0_1_North_when_executeCommand_given_location_0_0_N_and_command_move(){
        //given
        Location locationInitial = new Location(0,0,Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Move;
        //when
        Location locationAfterMove =marsRover.executeCommand(commandMove);
        //then
        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(1,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.North,locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_minus_one_South_when_executeCommand_given_location_0_0_S_and_command_move(){
        //given
        Location locationInitial = new Location(0,0,Direction.South);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Move;
        //when
        Location locationAfterMove =marsRover.executeCommand(commandMove);
        //then
        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(-1,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.South,locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_1_0_East_when_executeCommand_given_location_0_0_E_and_command_move(){
        //given
        Location locationInitial = new Location(0,0,Direction.East);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Move;
        //when
        Location locationAfterMove =marsRover.executeCommand(commandMove);
        //then
        Assertions.assertEquals(1,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.East,locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_minus_one_0_West_when_executeCommand_given_location_0_0_W_and_command_move(){
        //given
        Location locationInitial = new Location(0,0,Direction.West);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Move;
        //when
        Location locationAfterMove =marsRover.executeCommand(commandMove);
        //then
        Assertions.assertEquals(-1,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.West,locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_0_West_when_executeCommand_given_location_0_0_N_and_command_left(){
        //given
        Location locationInitial = new Location(0,0,Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Left;
        //when
        Location locationAfterMove =marsRover.executeCommand(commandMove);
        //then
        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.West,locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_0_East_when_executeCommand_given_location_0_0_N_and_command_right(){
        //given
        Location locationInitial = new Location(0,0,Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Right;
        //when
        Location locationAfterMove =marsRover.executeCommand(commandMove);
        //then
        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.East,locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_0_North_when_executeCommand_given_location_0_0_E_and_command_left(){
        //given
        Location locationInitial = new Location(0,0,Direction.East);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Left;
        //when
        Location locationAfterMove =marsRover.executeCommand(commandMove);
        //then
        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.North,locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_0_South_when_executeCommand_given_location_0_0_E_and_command_right(){
        //given
        Location locationInitial = new Location(0,0,Direction.East);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Right;
        //when
        Location locationAfterMove =marsRover.executeCommand(commandMove);
        //then
        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.South,locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_0_East_when_executeCommand_given_location_0_0_S_and_command_left(){
        //given
        Location locationInitial = new Location(0,0,Direction.South);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Left;
        //when
        Location locationAfterMove =marsRover.executeCommand(commandMove);
        //then
        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.East,locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_0_West_when_executeCommand_given_location_0_0_S_and_command_right(){
        //given
        Location locationInitial = new Location(0,0,Direction.South);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Right;
        //when
        Location locationAfterMove =marsRover.executeCommand(commandMove);
        //then
        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.West,locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_0_South_when_executeCommand_given_location_0_0_W_and_command_left(){
        //given
        Location locationInitial = new Location(0,0,Direction.West);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Left;
        //when
        Location locationAfterMove =marsRover.executeCommand(commandMove);
        //then
        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.South,locationAfterMove.getDirection());
    }

    @Test
    void should_change_to_0_0_North_when_executeCommand_given_location_0_0_W_and_command_right(){
        //given
        Location locationInitial = new Location(0,0,Direction.West);
        MarsRover marsRover = new MarsRover(locationInitial);
        Command commandMove = Command.Right;
        //when
        Location locationAfterMove =marsRover.executeCommand(commandMove);
        //then
        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.North,locationAfterMove.getDirection());
    }

    @Test
    void should_change_batch_when_executeCommand_given_location_0_0_N_and_command_something(){
        //given
        Location locationInitial = new Location(0,0,Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        List<Command> commands = new ArrayList<>();
        commands.add(Command.Move);
        commands.add(Command.Left);
        commands.add(Command.Move);
        commands.add(Command.Right);
        commands.add(Command.Move);
        commands.add(Command.Left);
        commands.add(Command.Move);
        //when
        Location locationAfterMove =marsRover.executeBatchCommand(commands);
        //then
        Assertions.assertEquals(-2,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(2,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.West,locationAfterMove.getDirection());
    }
}
